<#
.SYNOPSIS
This Script will copy relevant user information.

.DESCRIPTION
this takes a Windows user account object and collects the SID information for the account and all groups that
it belongs to. It uses this information to compile a list of NTFS access permissions to either a specified location
or by default it will collect information on the S:\ drive.

The output of this command is a powershell object that contains the file permissions and groups that
the user belongs to.

There are also a few helper methods to copy these permissions to other accounts. Copygroups() will assign all the
group memberships the current object has to a new user specified at calling time. CopyNTFS() will check the current
objects files for explicit permissions and assign these to the new account specified. Clone() will copy both groups
and NTFS permissions to the account specified. Export() allows you to export the permissions and groups to files
for later use. When calling Export() the first parameter is the folder to save everything in and the second optional
parameter specifies if you would like just the groups or just the permissions.

This Script relies on the Active Directory and NTFSSecurity modules to run. Please ensure they are available prior
to running this.

.PARAMETER SamAccountName
This is the user account that will be checked. It will accept pipelined input or can be called with the -User switch

.PARAMETER SharePath
this is the location the command will check NTFS permissions for. The default path is s:\

.PARAMETER Export
If you are running the script just to get a copy of the permissions and group members then add -Export <FilePath> to
your call and it will export two files to the path specified. one will be <SAMAccountName>_permissions.csv and the
other will be <SAMAccountName>_groups.csv

.EXAMPLE
Get-ExitingAccount.ps1 -SamAccountName "AUser"
this will collect all the groups that AUser belongs to and then will check the permissions available to those
groups and this user in s:\

.EXAMPLE
$example = Get-ExitingAccount.ps1 -SamAccountName "AUser"; $example.clone("BUser")
This first collects the information on the AUser account and stores it in a variable named '$example' then it uses
the clone method to assign the same groups and NTFS permissions to the 'BUser' account.

.Notes
Script: Get-ExitingAccount.ps1
Author: Mike Murray
Created: 12/19/2016
Last Edit: 11/30/18 Mike Murray
Comments: This is a very slow script.

#>

#Requires -Version 3.0
#Requires -Modules ActiveDirectory, NTFSSecurity

[cmdletBinding()]
[OutputType("PSCustomObject[]")]

Param(
    [Parameter(Mandatory = $True,ValueFromPipelineByPropertyName = $True)]
    [ValidateNotNullOrEmpty()]
    [Alias("User")]
    [string[]]$SamAccountName,
    [string]$SharePath = "s:\",
    [String]$Export
    )

Begin{
        Write-Progress -Activity "Collecting Files"
    #This command uses cmdlets from the NTFSSecurity module. for portability I would like to remove this later.
    $Files = Get-ChildItem2 $SharePath -Recurse |
             Get-NTFSInheritance | Where-Object{ $_.AccessInheritanceEnabled -eq $False}
    $Files += Get-NTFSInheritance $SharePath
    Write-Progress -Activity "Collecting Files" -Status "Done" -Completed
}

Process{
 foreach( $User in $SamAccountName){
    #There are easier ways to get the SID but the below commands account for an edge case where the SID has changed.
    $objNTUser = New-Object System.Security.Principal.NTAccount($User)
    $objuser = [pscustomobject]@{SID=$objNTUser.Translate([System.Security.Principal.SecurityIdentifier])}

    Write-Verbose "Collected user: $User SID as: $($objUser.SID.Value)"

    $objADUser = Get-ADUser -Identity $User -Properties *
    Add-Member -InputObject $objUser -NotePropertyName Groups -NotePropertyValue $($objADUser.memberof | Get-ADGroup)
    $grpSID = $($objADUser.Memberof | Get-ADGroup).sid.value
    $grpSID += $objUser.SID.Value
    Add-Member -InputObject $objUser -NotePropertyName SIDCollection -NotePropertyValue $grpSID
    Add-Member -InputObject $objUser -NotePropertyName SamAccountname -NotePropertyValue  $objADUser.SamAccountName
    Add-Member -InputObject $objUser -NotePropertyName Name -NotePropertyValue $objADUser.Name

    Write-Verbose "Collected user: $User Group Memberships as: $objUser.Groups"

     $usrPerm = @()

    # collecting file permissions
    foreach($file in $files){
        Write-Progress -Activity "Checking Rights" -Status "File: $($File.FullName)"

        foreach($SID in $grpSID){
            $perm = Get-NTFSAccess -Path $file.FullName -Account $SID
            if($perm){
                $usrPerm += $perm
                switch -Wildcard ($(Get-NTFSAccess -Path $File.FullName -Account $SID).AccessRights){
                    "*FullControl*" { $Full += $File.FullName + '; '}
                    "*ReadAndExecute*" {$RE += $file.FullName + '; '}
                    "*Modify*" { $Modify += $File.FullName + '; '}
                    "*Read*" { $Read += $File.FullName + '; '}
                    "*Write*" {$Write += $File.FullName + '; '}
                    "*List*" { $List += $File.FullName + '; '}
                    default { $Other += $File.FullName + '; '}
                }
            }
        }
    }
    Write-Progress -Activity "Checking Rights" -Status "Done" -Completed

    #now that we have the permissions we assign them to the correct properties.
    Add-Member -InputObject $objUser -NotePropertyName FullControlPerm -NotePropertyValue $Full
    Add-Member -InputObject $objUser -NotePropertyName ReadAndExecutePerm -NotePropertyValue $RE
    Add-Member -InputObject $objUser -NotePropertyName ModifyPerm -NotePropertyValue $Modify
    Add-Member -InputObject $objUser -NotePropertyName ReadPerm -NotePropertyValue $Read
    Add-Member -InputObject $objUser -NotePropertyName WritePerm -NotePropertyValue $Write
    Add-Member -InputObject $objUser -NotePropertyName ListPerm -NotePropertyValue $List
    Add-Member -InputObject $objUser -NotePropertyName OtherPerm -NotePropertyValue $Other
    Add-Member -InputObject $objuser -NotePropertyName AllPerm -NotePropertyValue $usrPerm

    # this defines the member methods used to copy permissions and groups to a new account.
    Add-Member -InputObject $objuser -MemberType ScriptMethod -Name CopyGroups -Value {
        param(
            [Parameter(Mandatory=$True, Position=0)]
            [string]$NewUser
            )

        foreach( $group in $this.Groups ){
            try{ Add-ADGroupMember -identity $Group -Members $NewUser }
            catch{ Write-Host $_.exception.Message "on" $group -BackgroundColor Black -ForegroundColor Red}
            }
        }

    Add-Member -InputObject $objuser -MemberType ScriptMethod -Name CopyNTFS -Value {
        param(
            [Parameter(Mandatory=$True, Position=0)]
            [string]$NewUser
            )

            foreach($ACE in $this.AllPerm ){
                if( $ace.account.accountname -match $this.SamAccountName ){
                    Add-NTFSAccess -Path $ACE.FullName -Account $NewUser -AccessRights $ACE.AccessRights `
                    -AccessType $ACE.AccessControlType -InheritanceFlags $ACE.InheritanceFlags `
                    -PropagationFlags $ACE.PropagationFlags
                }
            }
        }

    Add-Member -InputObject $objUser -MemberType ScriptMethod -Name Clone -Value {
        param(
            [Parameter(Mandatory=$True, Position=0)]
            [string]$NewUser
            )

        foreach($ACE in $this.AllPerm ){
            if( $ace.account.accountname -match $this.SamAccountName ){
                Add-NTFSAccess -Path $ACE.FullName -Account $NewUser -AccessRights $ACE.AccessRights `
                -AccessType $ACE.AccessControlType -InheritanceFlags $ACE.InheritanceFlags `
                -PropagationFlags $ACE.PropagationFlags
            }
        }

        foreach( $group in $this.Groups ){
            try{ Add-ADGroupMember -identity $Group -Members $NewUser }
            catch{ Write-Host $_.exception.Message "on" $group -BackgroundColor Black -ForegroundColor Red}
            }
        }

    Add-Member -InputObject $objUser -MemberType ScriptMethod -Name Export -Value {
        param(
            [Parameter(Mandatory=$True, Position=0)]
            [ValidateScript({Test-Path $_})]
            [String]$Path,
            [Parameter(Mandatory=$False, Position=1)]
            [ValidateSet('all','groups','permissions','perms','perm')]
            [String]$Type='all'
            )
        switch -wildcard ($type){
            'all'    { $this.AllPerm | Export-CSV -NoTypeInformation -Path "$Path\$($this.SamAccountName)_permissions.csv"
                       $this.Groups | Export-CSV -NoTypeInformation -Path "$Path\$($this.SamAccountName)_groups.csv"
                     }
            'groups' { $this.Groups | Export-CSV -NoTypeInformation -Path "$Path\$($this.SamAccountName)_groups.csv" }
            "per*"   { $this.AllPerm | Export-CSV -NoTypeInformation -Path "$Path\$($this.SamAccountName)_permissions.csv" }
            default  { write-error "$type is not a valid selection" }
        }
    }


   if($Export){
        $objUser.Export($export)
   }
   else{
        #output the user object
        $objUser
   }
}
}